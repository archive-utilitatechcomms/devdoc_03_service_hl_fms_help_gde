﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Multi-Factor Authentication</MicroContentPhrase>
        <MicroContentPhrase>Authenticator</MicroContentPhrase>
        <MicroContentPhrase>MFA</MicroContentPhrase>
        <MicroContentPhrase>Microsoft Authenticator app</MicroContentPhrase>
        <MicroContentPhrase>Authenticator app</MicroContentPhrase>
        <MicroContentPhrase>WatchGuard</MicroContentPhrase>
        <MicroContentPhrase>Work from home</MicroContentPhrase>
        <MicroContentPhrase>Working from home</MicroContentPhrase>
        <MicroContentPhrase>remote working</MicroContentPhrase>
        <MicroContentPhrase>VPN</MicroContentPhrase>
        <MicroContentPhrase>Remote Desktop Services</MicroContentPhrase>
        <MicroContentPhrase>RDS</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Process: Office 365 Multi-Factor Authentication (MFA)</xhtml:h1>
                    <xhtml:p>We’re constantly improving our remote access systems to enable more people to effectively work from home.</xhtml:p>
                    <xhtml:p>This process explains how to setup MFA (Multi-Factor Authentication) to help safeguard access to our data and applications, and to improve our security standards with a second form of authentication.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Setting Up MFA - Quick Setup</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>
                                <xhtml:img xhtml:src="00_IMG_Proc_MFA_00A.png" xhtml:style="width: 1400px;min-width: 1400px;max-width: 1400px;height: 739px;min-height: 739px;max-height: 739px;">
                                </xhtml:img>
                            </xhtml:p>
                            <xhtml:p>Anyone with their MS Authenticator set up to provide a one time password (6 digit code) by default will need to switch to using Push Notifications:</xhtml:p>
                            <xhtml:p>
                                <xhtml:img xhtml:src="00_IMG_Proc_MFA_00B.png" xhtml:style="height: 781px;min-height: 781px;max-height: 781px;width: 1400px;min-width: 1400px;max-width: 1400px;">
                                </xhtml:img>
                            </xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Setting Up MFA - Full Steps</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The Microsoft Authenticator app helps you sign in to your accounts if you use two-factor verification.</xhtml:p>
                            <xhtml:p>When working off site outside of Utilita premises or working from a Utilita Hub, you’ll need to verify your remote logon credentials using the Microsoft Authenticator app.</xhtml:p>
                            <xhtml:p>Two-factor verification helps you to access your accounts more securely, especially while viewing sensitive information. Because passwords can be forgotten, stolen, or compromised, two-factor verification is an additional security step that helps protect your account by making it harder for other people to break in.</xhtml:p>
                            <xhtml:p>You can use the Microsoft Authenticator app in multiple ways, including:</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>Respond to a prompt for authentication after you sign in with your username and password.</xhtml:li>
                                <xhtml:li>Sign in without entering a password, using your username, the authenticator app, and your mobile device with your fingerprint, face, or PIN.</xhtml:li>
                                <xhtml:li>As a code generator for any other accounts that support authenticator apps.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>You can find further information about the Microsoft Authenticator app <xhtml:a xhtml:href="https://docs.microsoft.com/en-us/azure/active-directory/user-help/user-help-auth-app-overview" xhtml:target="_blank" xhtml:title="MS Office 365 MFA  Help" xhtml:alt="MS Office 365 MFA  Help">here</xhtml:a>.</xhtml:p>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>1. Setting Up - Install Microsoft Authenticator</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>You must download the Microsoft Authenticator app on a mobile device from the following sites:</xhtml:p>
                                    <xhtml:ul>
                                        <xhtml:li>Google Android. On your Android device, go to Google Play to <xhtml:a xhtml:href="https://app.adjust.com/e3rxkc_7lfdtm?fallback=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.azure.authenticator" xhtml:target="_blank" xhtml:title="Download and install the Microsoft Authenticator app - Google Android" xhtml:alt="Download and install the Microsoft Authenticator app - Google Android">download and install the Microsoft Authenticator app</xhtml:a>.</xhtml:li>
                                        <xhtml:li>Apple iOS. On your Apple iOS device, go to the App Store to <xhtml:a xhtml:href="https://app.adjust.com/e3rxkc_7lfdtm?fallback=https%3A%2F%2Fitunes.apple.com%2Fus%2Fapp%2Fmicrosoft-authenticator%2Fid983156458" xhtml:target="_blank" xhtml:title="Download and install the Microsoft Authenticator app - Apple iOS" xhtml:alt="Download and install the Microsoft Authenticator app - Apple iOS">download and install the Microsoft Authenticator app</xhtml:a>.</xhtml:li>
                                    </xhtml:ul>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>2. Setting Up - Add your work account</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:ol>
                                        <xhtml:li>	On your computer, log into Office 365. If you haven’t set up MFA, the following screen will be displayed:</xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:blockquote>
                                        <xhtml:p>
                                            <xhtml:img xhtml:src="00_IMG_Proc_MFA_01.png" xhtml:class="Screen_Thumbnail">
                                            </xhtml:img>
                                        </xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:ol MadCap:continue="true">
                                        <xhtml:li>On the More information required screen, click the <xhtml:b>Next</xhtml:b> button. The following screen will be displayed:</xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:blockquote>
                                        <xhtml:p>
                                            <xhtml:img xhtml:src="00_IMG_Proc_MFA_02.png" xhtml:class="Screen_Thumbnail">
                                            </xhtml:img>
                                        </xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:blockquote>
                                        <xhtml:p>You will see that all options are greyed out and the system won’t allow you to select them. Click the <xhtml:b>Set up</xhtml:b> button.</xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:ol MadCap:continue="true">
                                        <xhtml:li>The Configure mobile app screen will appear. Keep this page open so you can see the account name and secret key if you need to.</xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:blockquote>
                                        <xhtml:p>You’ll need to scan the QR verification code (image) shortly.</xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:blockquote>
                                        <xhtml:p>
                                            <xhtml:img xhtml:src="00_IMG_Proc_MFA_03.png" xhtml:class="Screen_Thumbnail">
                                            </xhtml:img>
                                        </xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:blockquote>
                                        <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">We’re going to be using the QR verification code method to set up the system, so ignore the instruction in the above screen to enter the Account Name and Secret key. The Account Name and Secret key process is subject to error. If you’re unable to scan the QR verification barcode contact Utilita IT Helpdesk, phone: 01962 874 859.</xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>3.	Setting Up - Run Microsoft Authenticator</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:ol>
                                        <xhtml:li>Open the Microsoft Authenticator app on your Mobile device.  Select Add account from the Customize and control icon in the upper-right, and then select Other account (Google, Facebook, etc.).</xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:blockquote>
                                        <xhtml:p>
                                            <xhtml:img xhtml:src="00_IMG_Proc_MFA_04.png" xhtml:class="Screen_Thumbnail">
                                            </xhtml:img>
                                        </xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:blockquote>
                                        <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">If this is the first time you're setting up the Microsoft Authenticator app on your device, you might receive a prompt asking whether to allow the app to access your camera (iOS) or to allow the app to take pictures and record video (Android). You must select Allow so the authenticator app can access your camera to take a picture of the QR code in the next step.</xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:ol MadCap:continue="true">
                                        <xhtml:li>Use your device's camera to scan the QR code from the Configure mobile app screen on your computer.</xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:blockquote>
                                        <xhtml:p>
                                            <xhtml:img xhtml:src="00_IMG_Proc_MFA_05.png" xhtml:class="Screen_Thumbnail">
                                            </xhtml:img>
                                        </xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:ol MadCap:continue="true">
                                        <xhtml:li>If the system recognises the QR code, the Microsoft Authenticator app will automatically configure your account.</xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:blockquote>
                                        <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">If the camera on your mobile device is unable to capture QR codes, you can manually add your account information for two-factor verification. If you’re unable to scan the image, contact Utilita IT Helpdesk, phone: 01962 874 859.</xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:ol MadCap:continue="true">
                                        <xhtml:li>The Accounts screen shows your account name (usually your email address) and a six-digit verification code. For additional security, the verification code changes every 30 seconds preventing someone from using a code multiple times.</xhtml:li>
                                    </xhtml:ol>
                                    <xhtml:blockquote>
                                        <xhtml:p>Make a note of the verification code displayed in this window:</xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:blockquote>
                                        <xhtml:p>
                                            <xhtml:img xhtml:src="00_IMG_Proc_MFA_06.png" xhtml:class="Screen_Thumbnail">
                                            </xhtml:img>
                                        </xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:blockquote>
                                        <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">The verification codes provided for your account aren’t case-sensitive and don’t need spaces.</xhtml:p>
                                    </xhtml:blockquote>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Signing In using Microsoft Authenticator</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>After you’ve added your account to the app, you can sign in on your computer using the Authenticator app on your device.</xhtml:p>
                            <xhtml:ol>
                                <xhtml:li>On your computer, on the Configure Mobile App screen, click the Next button.</xhtml:li>
                            </xhtml:ol>
                            <xhtml:blockquote>
                                <xhtml:p>
                                    <xhtml:img xhtml:src="00_IMG_Proc_MFA_07.png" xhtml:class="Screen_Thumbnail">
                                    </xhtml:img>
                                </xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:ol MadCap:continue="true">
                                <xhtml:li>You’ll be taken back to the Additional Security Verification screen. click the Next button.</xhtml:li>
                            </xhtml:ol>
                            <xhtml:blockquote>
                                <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">Most of the options in this screen are greyed out and cannot be selected. You’re only able to select Next.</xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:blockquote>
                                <xhtml:p>
                                    <xhtml:img xhtml:src="00_IMG_Proc_MFA_08.png" xhtml:class="Screen_Thumbnail">
                                    </xhtml:img>
                                </xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:ol MadCap:continue="true">
                                <xhtml:li>The following verification page will be displayed:</xhtml:li>
                            </xhtml:ol>
                            <xhtml:blockquote>
                                <xhtml:p>
                                    <xhtml:img xhtml:src="00_IMG_Proc_MFA_09.png" xhtml:class="Screen_Thumbnail">
                                    </xhtml:img>
                                </xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:blockquote>
                                <xhtml:p>Enter the verification code displayed in the app in the box, and then click on the Verify button.</xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:blockquote>
                                <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">The verification codes provided for your account aren’t case-sensitive and don’t need spaces.</xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:blockquote>
                                <xhtml:p>If the verification is successful, click the Done button (for some users it might be the Finished button).</xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:blockquote>
                                <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">If verification is not successful contact Utilita IT Helpdesk, phone: 01962 874 859.</xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:ol MadCap:continue="true">
                                <xhtml:li>Within the portal you may be asked to add a recovery method. We recommend adding your work mobile number.</xhtml:li>
                            </xhtml:ol>
                            <xhtml:blockquote>
                                <xhtml:p>Enter your contact number and click the <xhtml:b>Next</xhtml:b> button.</xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:blockquote>
                                <xhtml:p>
                                    <xhtml:img xhtml:src="00_IMG_Proc_MFA_10.png">
                                    </xhtml:img>
                                </xhtml:p>
                            </xhtml:blockquote>
                            <xhtml:p>You’ve now configured your multi-factor authentication.</xhtml:p>
                            <xhtml:p>You’ll now be able to launch the required apps, for instance, run Outlook, MS Office 365, Skype, or Teams.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Sign In and Authentication - Outlook</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>The next time you sign in and authenticate into Outlook, you’ll need to enter your MFA code.</xhtml:p>
                                    <xhtml:p>When the system displays the Account Information screen, you’ll be prompted to enter the code.</xhtml:p>
                                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">Outlook and Skype can take up to 30 minutes to prompt for your application password.</xhtml:p>
                                    <xhtml:p>Run MS Authenticator on your mobile device to get the latest MFA code, and then enter the code into the Account Information screen. Click the Verify button.</xhtml:p>
                                    <xhtml:p>
                                        <xhtml:img xhtml:src="00_IMG_Proc_MFA_11.png" xhtml:class="Screen_Thumbnail">
                                        </xhtml:img>
                                    </xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <MadCap:dropDown>
                                <MadCap:dropDownHead>
                                    <MadCap:dropDownHotspot>Sign In and Authentication - Skype</MadCap:dropDownHotspot>
                                </MadCap:dropDownHead>
                                <MadCap:dropDownBody>
                                    <xhtml:p>The next time you sign in and authenticate into Skype, you’ll need to enter your MFA code.</xhtml:p>
                                    <xhtml:p>When the Skype for Business screen is displayed you will be prompted to enter the code.</xhtml:p>
                                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">Outlook and Skype can take up to 30 minutes to prompt for your application password.</xhtml:p>
                                    <xhtml:p>Run MS Authenticator on your mobile device to get the latest MFA code, and then enter the code and click the Verify button.</xhtml:p>
                                    <xhtml:p>
                                        <xhtml:img xhtml:src="00_IMG_Proc_MFA_12.png" xhtml:class="Screen_Thumbnail">
                                        </xhtml:img>
                                    </xhtml:p>
                                    <xhtml:p>&#160;</xhtml:p>
                                </MadCap:dropDownBody>
                            </MadCap:dropDown>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>You can find further information about the Microsoft Authenticator app <xhtml:a xhtml:href="https://docs.microsoft.com/en-us/azure/active-directory/user-help/user-help-auth-app-overview" xhtml:target="_blank" xhtml:title="Microsoft Authenticator App Help Guide" xhtml:alt="Microsoft Authenticator App Help Guide">here</xhtml:a>.</xhtml:p>
                    <xhtml:p>We also have a dedicated  <xhtml:a xhtml:href="https://utilita.sharepoint.com/sites/itknowledgebase/Pages/Home Pages/Public Home Pages/Working from Home FAQ.aspx" xhtml:target="_blank" xhtml:title="Working From Home FAQs page" xhtml:alt="Working From Home FAQs page">Working From Home FAQs page</xhtml:a>, that shows you how to contact the IT Service Desk, troubleshooting VPNs, resetting passwords, and more.</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>