﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Bitbucket</MicroContentPhrase>
        <MicroContentPhrase>Source Control</MicroContentPhrase>
        <MicroContentPhrase>Git</MicroContentPhrase>
        <MicroContentPhrase>Atlassian</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Tool: Bitbucket (Source Control)</xhtml:h1>
                    <xhtml:p><xhtml:a xhtml:href="https://www.atlassian.com/software/bitbucket" xhtml:target="_blank" xhtml:title="Bitbucket " xhtml:alt="Bitbucket ">Bitbucket</xhtml:a> is a web-based version control repository hosting service owned by <xhtml:a xhtml:href="https://www.atlassian.com/" xhtml:target="_blank" xhtml:title="Atlassian" xhtml:alt="Atlassian">Atlassian</xhtml:a>, for source code and development projects that use either Mercurial (since launch till June 1, 2020 or Git (since October 201 revision control systems. Bitbucket offers both commercial plans and free accounts. It offers free accounts with an unlimited number of private repositories (which can have up to five users in the case of free accounts) as of September 2010. Bitbucket integrates with other Atlassian software like Jira, HipChat, Confluence and Bamboo.</xhtml:p>
                    <xhtml:p>It is similar to GitHub, which primarily uses Git. Bitbucket has traditionally marketed its services to professional developers with private proprietary software code, especially since being acquired by Atlassian in 2010. In February 2017, Bitbucket announced it had reached 6 million developers and 1 million teams on its platform. In April 2019 Atlassian announced that Bitbucket reached 10 million registered users and over 28 million repositories.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Build quality software with code review</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Approve code review more efficiently with pull requests. Create a merge checklist with designated approvers and hold discussions right in the source code with inline comments.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Deploy often with built-in continuous delivery</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Bitbucket Pipelines with Deployments lets you build, test and deploy with integrated CI/CD. Benefit from configuration as code and fast feedback loops.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Secure your workflow</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Know your code is secure in the Cloud with IP whitelisting and required 2-step verification. Restrict access to certain users, and control their actions with branch permissions and merge checks for quality code.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Code Review</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Continuously collaborate, merge with confidence, and deliver quality code.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Branch Permissions</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Provide granular access control for your team, ensuring the right people have the right access to your code.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Pipelines</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Bitbucket’s integrated CI/CD tool that enables you to build and test automatically and deploy with confidence.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>