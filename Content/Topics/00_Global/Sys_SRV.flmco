<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="srv">
        <MicroContentPhrase>SRV</MicroContentPhrase>
        <MicroContentPhrase>Sales and Registration Validation</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>System: Sales and Registration Validation (SRV)</xhtml:h1>
                    <xhtml:p>The SRV is used to input information relating to new customers who are signing up for Utilita to provide their gas or electricity (or both). It records new sales, validating fields as they are completed. Assuming all validation passes, the customer is then eligible for registration. SRV only accepts domestic customers with existing pre-pay meters already installed. Any other type of customer (commercial, credit, etc.) fails the validation checks.</xhtml:p>
                    <xhtml:p>You can find up to date and detailed documentation of SRV, including ​user guidance in the following links:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li xhtml:href="../../../../DevDoc_03_WebMob_HL_SRV_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="SRV&#160;DevOps Help Guide" xhtml:alt="SRV&#160;DevOps Help Guide"><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_WebMob_HL_SRV_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="SRV DevOps Help Guide" xhtml:alt="SRV DevOps Help Guide">SRV&#160;DevOps Help Guide</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/TechPubs/Help_SRV/Content/Topics/1.0_welcome_files/welcome_to_srv_and_the_help_system.htm" xhtml:title="SRV End-User Help Guide" xhtml:alt="SRV End-User Help Guide" xhtml:target="_blank">SRV End-User Help Guide</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_WebMob_HL_SRV_Help_REL/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="SRV Release Information" xhtml:alt="SRV Release Information" xhtml:target="_blank">SRV Release Information</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>There are four possible routes for data to be entered into SRV:</xhtml:p>
                    <xhtml:ol>
                        <xhtml:li>Directly input by a member of the telesales team.</xhtml:li>
                        <xhtml:li>Input by door-to-door sales agents via the tablet app.</xhtml:li>
                        <xhtml:li>Input by outsourced 3rd party telesales staff.</xhtml:li>
                        <xhtml:li>Provided by online switching sites such as Uswitch (although Utilita don't tend to use such sites extensively as they're expensive in relation to the cost of telesales or door-to-door sales).</xhtml:li>
                    </xhtml:ol>
                    <xhtml:p>Once a new sale is entered into SRV, a pre-defined cooling off period is automatically triggered. This is typically 7 days, allowing the customer to cancel without penalty if they want to.</xhtml:p>
                    <xhtml:p>Every sale is attributed to a sales agent to monitor performance (individual, team and regional) and to calculate commission due on those sales.</xhtml:p>
                    <xhtml:p>Every page of the contract pads used in the field by the sales team has a unique application number. When the customer details are submitted, a unique customer ID is generated. This ID is generated automatically when entering customer details into the application directly, rather than having to use a contract pad.</xhtml:p>
                    <xhtml:p>Validation is provided by an API that accesses the GB Data real time database which contains supply data of merged gas and electricity customer databases (other customer databases have only one set of customers or the other, not both combined). This is a valuable service as address data can be different for gas and electricity customers.</xhtml:p>
                    <xhtml:p>If an application fails validation, an administrator user has the authority to go into the system to investigate why the validation failed and potentially pass them as a customer regardless.</xhtml:p>
                    <xhtml:p>At the end of the cooling off period, the customer is eligible for registration and they are added to the <xhtml:a xhtml:href="CRMApp.htm" xhtml:target="_blank" xhtml:title="Customer Relationship Management (CRM) " xhtml:alt="Customer Relationship Management (CRM) ">CRM</xhtml:a> database. All data is then available through CRM for customer support and service calls.</xhtml:p>
                    <xhtml:p>
                        <xhtml:img xhtml:src="00_IMG_Arch_SRV.png" xhtml:class="Screen_Thumbnail" />
                    </xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>