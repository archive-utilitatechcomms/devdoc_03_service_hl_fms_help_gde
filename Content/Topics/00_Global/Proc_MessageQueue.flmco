﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Message Queue</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Process: Message Queue</xhtml:h1>
                    <xhtml:p>A message queue is a form of asynchronous service-to-service communication used in serverless and microservices architectures. Messages are stored on the queue until they are processed and deleted. Each message is processed only once, by a single consumer. Message queues can be used to decouple heavyweight processing, to buffer or batch work, and to smooth spiky workloads.</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>Message queues and mailboxes are software-engineering components used for inter-process communication (IPC), or for inter-thread communication within the same process. They use a queue for messaging – the passing of control or of content. Group communication systems provide similar kinds of functionality.</xhtml:li>
                        <xhtml:li>The message queue paradigm is a sibling of the publisher/subscriber pattern, and is typically one part of a larger message-oriented middleware system. Most messaging systems support both the publisher/subscriber and message queue models in their API, e.g. Java Message Service (JMS).</xhtml:li>
                        <xhtml:li>Message queues provide an asynchronous communications protocol, meaning that the sender and receiver of the message do not need to interact with the message queue at the same time. Messages placed onto the queue are stored until the recipient retrieves them. Message queues have implicit or explicit limits on the size of data that may be transmitted in a single message and the number of messages that may remain outstanding on the queue.</xhtml:li>
                        <xhtml:li>Many implementations of message queues function internally: within an operating system or within an application. Such queues exist for the purposes of that system only.</xhtml:li>
                        <xhtml:li>In a typical message-queueing implementation, a system administrator installs and configures message-queueing software (a queue manager or broker), and defines a named message queue. Or they register with a message queuing service.</xhtml:li>
                        <xhtml:li>An application then registers a software routine that "listens" for messages placed onto the queue.</xhtml:li>
                        <xhtml:li>Second and subsequent applications may connect to the queue and transfer a message onto it.</xhtml:li>
                        <xhtml:li>The queue-manager software stores the messages until a receiving application connects and then calls the registered software routine. The receiving application then processes the message in an appropriate manner.</xhtml:li>
                    </xhtml:ul>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>