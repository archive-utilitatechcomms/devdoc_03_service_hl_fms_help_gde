﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="jbs">
        <MicroContentPhrase>JBS</MicroContentPhrase>
        <MicroContentPhrase>Job Booking System</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>System: Job Booking System (JBS)</xhtml:h1>
                    <xhtml:p>JBS is a web-based application used by Utilita and their sub-contractors (engineers / installers) to schedule and manage the installation of meters in the field. You can find more information in the following links:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_WebMob_HL_JBS_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="JBS DevOps Help Guide" xhtml:alt="JBS DevOps Help Guide" xhtml:target="_blank">JBS DevOps Help Guide</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/TechPubs/Help_JBS/Content/Topics/1.0_welcome_files/welcome_to_jbs_and_the_help_system.htm" xhtml:title="JBS&#160;End-User Help Guide" xhtml:alt="JBS&#160;End-User Help Guide" xhtml:target="_blank">JBS&#160;End-User Help Guide</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_WebMob_HL_JBS_Help_REL/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="JBS Release Information" xhtml:alt="JBS Release Information">JBS Release Information</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>It enables engineers to login and access a dashboard of jobs booked for today (or future dates) including details of each job such as the address, customer details, type of installation (gas, electric, dual fuel) and so on.</xhtml:p>
                    <xhtml:p>Admin users have access to a wider overview of all booked jobs and associated status information, including the number of engineers onsite, who's doing which job, when they are due to end by, etc.</xhtml:p>
                    <xhtml:p>The engineer uses the tablet app to update their status on each job, recording when they are on route, have completed, etc. This information is updated in the Production DB, which in turn triggers a message to be sent to the customer so that they know the current status of their booking.</xhtml:p>
                    <xhtml:p>Scheduling teams can use JBS to get an overview of resource capacity on any given day or geographic area, current bookings, who to allocate where, etc.</xhtml:p>
                    <xhtml:p>Once a meter has been installed on site, the Production DB contacts the <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_HL_B2B_SMSO-HES_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="B2B SMSO DevOps Help Guide" xhtml:alt="B2B SMSO DevOps Help Guide">Web Service Engine (WSE)</xhtml:a> to register the initial installation top up, provide credit information and whatever else is needed to get the customer's meter up and running.</xhtml:p>
                    <xhtml:p>
                        <xhtml:img xhtml:src="00_IMG_Arch_JBS.png" xhtml:class="Screen_Thumbnail">
                        </xhtml:img>
                    </xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>