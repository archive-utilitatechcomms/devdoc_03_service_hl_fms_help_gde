﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="10" MadCap:lastHeight="1376" MadCap:lastWidth="982">
    <head>
        <link href="../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Object - <a name="TMeterRegisters"></a><MadCap:keyword term="TMeterRegisters" />TMeterRegisters</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code>TMeterRegisters</code> - the structure of this object is described below:</p>
                <table style="width: 100%;mc-table-style: url('../../../TableStyles/TabMenuMap.css');margin-left: 0;margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 142px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 495px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 163px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 180px;" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveImport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveImport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveImport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveImport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveExport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveExport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveExport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveExport.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_GasVolume.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_GasVolume.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_CurrencyCode.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_CurrencyCode.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_AccountBalance.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_AccountBalance.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_TotalOutstandingDebt.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_TotalOutstandingDebt.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveImportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveImportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveImportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveImportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveExportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveExportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveExportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveExportElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveImportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveImportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveImportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveImportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveExportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveExportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveExportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveExportElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveImportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveImportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveImportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveImportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActiveExportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActiveExportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactiveExportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactiveExportElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>