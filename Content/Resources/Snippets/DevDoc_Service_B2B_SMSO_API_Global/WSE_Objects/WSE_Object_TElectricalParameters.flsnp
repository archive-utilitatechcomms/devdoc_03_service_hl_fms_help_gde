﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="10" MadCap:lastHeight="511" MadCap:lastWidth="982">
    <head>
        <link href="../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Object - <a name="TElectricalParameters"></a><MadCap:keyword term="TElectricalParameters" />TElectricalParameters</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code>TElectricalParameters</code> - the structure of this object is described below:</p>
                <table style="width: 100%; mc-table-style: url('../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 142px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 495px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 163px;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 180px;" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_SupplyFrequency.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_SupplyFrequency.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_AveragePowerFactor.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_AveragePowerFactor.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ActivePower.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ActivePower.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ReactivePower.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ReactivePower.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_ApparentPower.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_ApparentPower.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_Double.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_PhaseElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_PhaseElement1.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_TPhaseParameters.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_PhaseElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_PhaseElement2.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_TPhaseParameters.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Field/WSE_Field_PhaseElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Desc/WSE_Desc_PhaseElement3.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Type/WSE_Type_TPhaseParameters.flsnp" />
                            </td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                <MadCap:snippetBlock src="../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>