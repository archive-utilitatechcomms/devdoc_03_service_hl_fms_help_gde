﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="12" MadCap:lastHeight="1717" MadCap:lastWidth="1752.667">
    <head>
        <link href="../../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Diagnostic_Services_-_Get_Heartbeats"></a>
                    <MadCap:keyword term="Diagnostic Services#;Get Heartbeats;THeartbeatInfo;GetHeartbeats;TServiceRequest" />Diagnostic Services - Get Heartbeats</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code>GetHeartbeats</code>: This service is used to get heartbeats received from the gateway. </p>
                <p>The gateway monitors devices and logs diagnostic parameters for analysing WAN and HAN communications. The gateway sends heartbeats at a predefined schedule frequency (normally once a day after midnight).</p>
                <p>WSE stores the previous nights heartbeats in its database and provides the same for diagnostic purposes.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Signature</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <table style="width: 100%; mc-table-style: url('../../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 44px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 16px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 1570px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 36px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1" colspan="9">Get Heartbeats</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve">POST</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve"><span style="color: #0000ff;">THeartbeatInfo</span> GetHeartbeats (<span style="color: #0000ff;">TServiceRequest</span> request)</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">&#160;</td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Parameter</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">request      </pre>
                            </div>
                            <div><pre class="code">(TServiceRequest)</pre>
                            </div>
                        </div>
                        <p>Refer to object <a href="../../../../../Topics/02_Dev_API/02_Dev_API_05_DataDef-2_WSE_Objects.htm#Objects_-_S" target="_blank" title="Object - TServiceRequest" alt="Object - TServiceRequest">TServiceRequest</a>.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Return Value</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">(THeartbeatInfo)</pre>
                            </div>
                        </div>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Structure - THeartbeatInfo</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>This object's structure is described below:</p>
                        <table style="width: 100%; mc-table-style: url('../../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 250px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 478px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 169px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 188px;">
                            </col>
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><code>WseId</code>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_WseId.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1"><code>LastContactTime</code>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_LastContactTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_DateTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><code>Heartbeats</code>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_Heartbeats.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_THeartbeat.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Structure - THeartbeat</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>This object's structure is described below:</p>
                        <table style="width: 100%; mc-table-style: url('../../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 233px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 495px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 169px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 188px;">
                            </col>
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_WseTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_WseTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_DateTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_GatewayTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_GatewayTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_DateTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_TimeZoneOffset.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_TimeZoneOffset.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_OperatorCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_OperatorCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_Rssi.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_Rssi.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_LastDayUnregisteredDuration.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_LastDayUnregisteredDuration.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_LastDayWanAttempts.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_LastDayWanAttempts.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_LastDayWanFailedAttempts.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_LastDayWanFailedAttempts.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_LastDayWanTxBytes.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_LastDayWanTxBytes.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_LastDayWanRxBytes.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_LastDayWanRxBytes.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_ConnectedDevices.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_ConnectedDevices.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_TDeviceCommsInfo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_WanNetworkType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_WanNetworkType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_WanAntennaType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_WanAntennaType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_HanAntennaType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_HanAntennaType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_No.flsnp" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>