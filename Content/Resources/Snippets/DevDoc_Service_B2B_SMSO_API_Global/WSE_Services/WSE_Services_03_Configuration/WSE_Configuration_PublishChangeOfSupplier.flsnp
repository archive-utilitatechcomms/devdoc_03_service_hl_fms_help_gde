﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="12" MadCap:lastHeight="1892" MadCap:lastWidth="1740">
    <head>
        <link href="../../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Configuration_Services_-_Publish_Change_of_Supplier"></a>
                    <MadCap:keyword term="Configuration Services;Publish Change of Supplier;PublishChangeOfSupplier;TCosRequest" />Configuration Services - Publish Change of Supplier</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code>PublishChangeOfSupplier</code>: This service is used to publish the change of supplier on a specified day.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Signature</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <table style="width: 100%; mc-table-style: url('../../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 44px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 14px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 1572px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 36px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1" colspan="9">Publish Change of Supplier</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve">POST</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve"><span style="color: #0000ff;">Integer</span> PublishChangeOfSupplier (<span style="color: #0000ff;">TCosRequest</span> request);
</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">&#160;</td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Parameter</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">request </pre>
                            </div>
                            <div><pre class="code">(TCosRequest)</pre>
                            </div>
                        </div>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Return Value</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">(Integer) </pre>
                            </div>
                            <div>
                                <p>&#160;</p>
                            </div>
                        </div>
                        <p>This service returns the WSE transaction ID that's assigned to the request.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Task Reply</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>HES must get a result of this task by calling the “<code><a href="../../../../../Topics/02_Dev_API/02_Dev_API_04_WSE-Services-Config.htm#Get" target="_blank" title="Configuration Services - Get Task Reply" alt="Configuration Services - Get Task Reply">GetTaskReply</a></code>” service with the WSE transaction Id.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Structure</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>This object's structure is described below:</p>
                        <table style="width: 100%; mc-table-style: url('../../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 142px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 495px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 163px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 180px;" />
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_HesId.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_HesId.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_Priority.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_Priority.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_SupplierChangeTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_SupplierChangeTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_DateTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">No</td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_SupplierCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_SupplierCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Yes</td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_ActionControl.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_ActionControl.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_TProposedActionControl.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">No</td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>