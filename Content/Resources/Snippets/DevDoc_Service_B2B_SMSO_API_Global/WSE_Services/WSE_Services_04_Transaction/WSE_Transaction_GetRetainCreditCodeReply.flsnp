﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="12" MadCap:lastHeight="2163" MadCap:lastWidth="1732.667">
    <head>
        <link href="../../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Transaction_Services_-_Get_Retain_Credit_Code_Reply"></a>
                    <MadCap:keyword term="Transaction Services;Get Retain Credit Code Reply;TRetainCreditResponse;GetRetainCreditCodeReply" />Transaction Services - Get Retain Credit Code Reply</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code>GetRetainCreditCodeReply</code>: This service is used to get a “Get Retain Credit Code” request response, which the meter processes.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Signature</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <table style="width: 100%; mc-table-style: url('../../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 44px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 12px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 1574px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 36px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1" colspan="9">Get Retain Credit Code Reply</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve">GET</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve"><span style="color: #0000ff;">TRetainCreditResponse</span> GetRetainCreditCodeReply (<span style="color: #0000ff;">Integer</span> wseId);</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">&#160;</td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Parameter</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">wseId </pre>
                            </div>
                            <div><pre class="code">(Integer)</pre>
                            </div>
                        </div>
                        <p>This is the WSE transaction ID assigned to the service request.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Return Value</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">(TRetainCreditResponse)</pre>
                            </div>
                        </div>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Structure</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>This object's structure is described below:</p>
                        <table style="width: 100%; mc-table-style: url('../../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 142px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 495px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 163px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 180px;" />
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_ProcessTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_ProcessTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_DateTime.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_CurrencyCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_CurrencyCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_AccountBalance.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_AccountBalance.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_RefundAmount.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_RefundAmount.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Field/WSE_Field_RefundCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Desc/WSE_Desc_RefundCode.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Type/WSE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Elements/Tab_Man/WSE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>