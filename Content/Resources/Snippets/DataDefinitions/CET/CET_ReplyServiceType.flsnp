﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="8" MadCap:lastHeight="879" MadCap:lastWidth="1087">
    <head>
        <link href="../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Constants_and_Enumerated_Types"></a>Constants and Enumerated Types -  <MadCap:keyword term="ReplyServiceType;Reply Service Type;reply service type" /><a name="ReplyServiceType"></a>ReplyServiceType</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code><MadCap:keyword term="ReplyServiceType" />ReplyServiceType</code>: Specifies the type of service to be used in order to get a request response sent to the device.</p>
                <table style="width: 100%; mc-table-style: url('../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 0.5%;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Value</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5000</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Task Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5001</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Vend Code Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5002</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Supply Control Code Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5003</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Register Reading Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5004</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Snapshot Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5005</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Meter Configuration Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5006</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Profile Configuration Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5007</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Event Configuration Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5008</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Dst Configuration Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5009</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Profile Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5010</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Meter Events Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5011</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Module Events Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5012</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Gateway Events Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5013</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Schedule Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5014</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Meter Diagnostic Data Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5018</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Engineering Utrn Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5019</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Retain Credit Code Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5020</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get System Parameter Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5022</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Demand Limit Configuration Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5023</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Account Details Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5024</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Reserved.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5025</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Get Utrn Code Reply.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">5026</td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">Get Mirror Data Reply.</td>
                        </tr>
                    </tbody>
                </table>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>