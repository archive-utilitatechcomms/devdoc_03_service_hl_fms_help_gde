﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="7" MadCap:lastHeight="848" MadCap:lastWidth="890">
    <head>
        <link href="../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>
                    <a name="Constants_and_Enumerated_Types">
                    </a>Constants and Enumerated Types - <MadCap:keyword term="EngineeringAction;Engineering Action;engineering action" /><a name="EngineeringAction"></a>EngineeringAction</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>
                    <code>
                        <MadCap:keyword term="EngineeringAction" />EngineeringAction</code>: Specifies the engineering action to be taken.</p>
                <table style="width: 100%;mc-table-style: url('../../../TableStyles/TabMenuMap.css');margin-left: 0;margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 0.5%;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Value</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">0</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Switch to Default Mode.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">1</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Force Connect if Armed.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">2</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Enable Local Port.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">3</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Disable Local Port.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">4</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Enable HAN Joining.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Disable HAN Joining.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">6</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Leave HAN.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">7</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Site ID and Payment Card Id.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">8</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Snapshots.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">9</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Profile Data.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">10</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Hardware Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">11</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Supply Quality Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">12</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Operating Condition Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">13</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Configuration Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">14</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Tamper Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">15</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Supply Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">16</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear UTRN log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">17</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Prepayment Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">18</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Account Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">19</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Clear Communication Log.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">20</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Reset Energy Registers.</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">23</td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">Enable Pulse Test.</td>
                        </tr>
                    </tbody>
                </table>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>