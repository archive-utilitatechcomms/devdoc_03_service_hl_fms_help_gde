﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="8" MadCap:lastHeight="755" MadCap:lastWidth="1087">
    <head>
        <link href="../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Constants_and_Enumerated_Types"></a>Constants and Enumerated Types - <MadCap:keyword term="EventLogType;Event Log Type;event log type" /><a name="EventLogType"></a>EventLogType</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code><MadCap:keyword term="EventLogType" />EventLogType</code>: Specifies the meter's event log types, meter communication module, and gateway.</p>
                <table style="width: 100%; mc-table-style: url('../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                    <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 0.5%;" />
                    <col class="TableStyle-TabMenuMap-Column-Column1" />
                    <thead>
                        <tr class="TableStyle-TabMenuMap-Head-Header1">
                            <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Value</th>
                            <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">0</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Hardware Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">1</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter Supply Quality Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">2</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Operating Condition Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">3</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Configuration Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">4</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Tamper Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">5</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Supply Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">6</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 UTRN Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">7</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Prepayment Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">8</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Credit Account Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">9</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Communication Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">10</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Meter
 Security Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">100</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Module
 Hardware Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">101</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Module
 Communication Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">102</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Module
 OTA Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">200</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Gateway
 Hardware Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">201</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Gateway
 Communication Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">202</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Gateway
 OTA Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">203</td>
                            <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">Gateway
 Configuration Log</td>
                        </tr>
                        <tr class="TableStyle-TabMenuMap-Body-Body1">
                            <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">204</td>
                            <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">Gateway
 Security Log</td>
                        </tr>
                    </tbody>
                </table>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>