﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="12" MadCap:lastHeight="1459" MadCap:lastWidth="1730.667">
    <head>
        <link href="../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Configuration_Services_-_Adjust_Meter_Time"></a>
                    <MadCap:keyword term="Configuration Services;Adjust Meter Time;AdjustMeterTime;TAdjustTimeRequest" />Configuration Services - Adjust Meter Time</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code>AdjustMeterTime</code>: This service is used to adjust the meter time.</p>
                <p>Whenever there is a significant time drift in a metering device that cannot be corrected without intervention of HES, WSE will notify this as an alarm. HES can get time drift using the <b><a href="../../../../Topics/02_Dev_API/02_Dev_API_04_WSE-Services-DataCollect.htm#Get" target="_blank" title="Data Collect Services - Get Time Drift Alarm" alt="Data Collect Services - Get Time Drift Alarm">Get Time Drift Alarm Reply</a></b> service. This service can be used to adjust the meter time by the given time drift.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Signature</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <table style="width: 100%; mc-table-style: url('../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 44px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 12px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 1574px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 36px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" />
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1" colspan="9">Adjust Meter Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve">POST</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve">
                                            <span style="color: #0000ff;">Integer</span> AdjustMeterTime (<span style="color: #0000ff;">TAdjustTimeRequest</span> request);</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">&#160;</td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Parameter</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">request  </pre>
                            </div>
                            <div><pre class="code">(TAdjustTimeRequest)</pre>
                            </div>
                        </div>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Return Value</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">(Integer) </pre>
                            </div>
                            <div>
                                <p>&#160;</p>
                            </div>
                        </div>
                        <p>This service returns a WSE transaction ID that is assigned to the request..</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Task Reply</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>HES must get a result of this task by calling the “<code><a href="../../../../Topics/02_Dev_API/02_Dev_API_04_WSE-Services-Config.htm#Get" target="_blank" title="Configuration Services - Get Task Reply" alt="Configuration Services - Get Task Reply">GetTaskReply</a></code>” service with the WSE transaction Id.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Structure</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>This object's structure is described below:</p>
                        <table style="width: 100%; mc-table-style: url('../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 141px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 496px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 163px;" />
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 180px;" />
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_HesId.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_HesId.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_Priority.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_Priority.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_TimeCorrection.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_TimeCorrection.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_TimeZoneOffset.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_TimeZoneOffset.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_No.flsnp" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>