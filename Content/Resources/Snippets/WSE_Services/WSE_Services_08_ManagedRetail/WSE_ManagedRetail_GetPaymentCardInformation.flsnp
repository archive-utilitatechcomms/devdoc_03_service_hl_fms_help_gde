﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:lastBlockDepth="12" MadCap:lastHeight="1625" MadCap:lastWidth="1752.667">
    <head>
        <link href="../../../TableStyles/TabMenuMap.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Managed_Retail_Services_-_Get_Payment_Card_Information"></a>
                    <MadCap:keyword term="Managed Retail Services;Get Payment Card Information;GetPaymentCardInformation;TPaymentCardInfo;TPaymentCardRequest" />Managed Retail Services - Get Payment Card Information</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p><code>GetPaymentCardInformation</code>: This service is used to get the current information stored against a “Payment Card”.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Signature</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <table style="width: 100%; mc-table-style: url('../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 44px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 16px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 1570px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 36px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1">
                            </col>
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1" colspan="9">Get Payment Card Information</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve">POST</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1"><pre class="code" xml:space="preserve"><span style="color: #0000ff;">TPaymentCardInfo</span> GetPaymentCardInformation (<span style="color: #0000ff;">TPaymentCardRequest</span> request);</pre>
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">&#160;</td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">&#160;</td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Parameter</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">request    </pre>
                            </div>
                            <div><pre class="code">(TPaymentCardRequest)</pre>
                            </div>
                        </div>
                        <p>Refer to object <code><a href="../../../../Topics/02_Dev_API/02_Dev_API_05_DataDef-2_Objects.htm#Objects_-_P" target="_blank" title="Object - TPaymentCardRequest" alt="Object - TPaymentCardRequest">TPaymentCardRequest</a></code>.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Return Value</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <div class="RespLayoutNewRowClass1">
                            <div><pre class="code" xml:space="preserve">(TPaymentCardInfo)</pre>
                            </div>
                        </div>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Structure</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>This object's structure is described below:</p>
                        <table style="width: 100%; mc-table-style: url('../../../TableStyles/TabMenuMap.css'); margin-left: 0; margin-right: auto;" class="TableStyle-TabMenuMap" cellspacing="0">
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 233px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 495px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 169px;">
                            </col>
                            <col class="TableStyle-TabMenuMap-Column-Column1" style="width: 188px;">
                            </col>
                            <thead>
                                <tr class="TableStyle-TabMenuMap-Head-Header1">
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Field Name</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Description</th>
                                    <th class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Type</th>
                                    <th class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Is Mandatory</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_WseId.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_WseId.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_SupplyType.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_ServicePointNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_DeviceNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_CustomerName.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_CustomerName.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_String.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_VatGroupNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_VatGroupNo.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_No.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_OutstandingDebt.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_OutstandingDebt.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_DebtRecoveryRate.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_DebtRecoveryRate.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyE-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Integer.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                                <tr class="TableStyle-TabMenuMap-Body-Body1">
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Field/SE_Field_Status.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Desc/SE_Desc_Status.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyB-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Type/SE_Type_Enum.flsnp" />
                                    </td>
                                    <td class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                        <MadCap:snippetBlock src="../../WSE_Services_TabStructureElements/Tab_Man/SE_Man_Yes.flsnp" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
    </body>
</html>